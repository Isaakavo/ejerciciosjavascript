//1. Tiene dos arreglos String, 'clientes y 'empleados, y deseas
//combinarlas para crear un arreglo de constactos.
//¿Que metodo sería el más adecuado para esta tarea?
//Prueba todas las opciones y describe el resultado

//a. concat
//b. join
//c. push
//d. splice

var clientes = ['juan', 'ernesto', 'maria'];
var empleados = ['Joahana', 'Elvis', 'Roberto'];

//concatena los arreglos después del ultimo elemento del arreglo que
//llama la funcion
console.log(clientes.concat(empleados));

//Concatena los nombres usando como separador los elementos de empleados
console.log(clientes.join(empleados));

//Agrega al final de clientes otro arreglo con empleados como valores
clientes.push(empleados);
console.log(clientes);

//Remueve o agrega elementos dependiendo de la funcion pasada
console.log(clientes.splice(empleados));

//La mas adecuada seria concat ya que te permite tener un
//'arreglo plano' con el cual es más facil trabajar

//2. Teniendo el siguiente arreglo y la siguiente funcion, agrega
//una variable para alojar el resultado de la función filter() y explica
//cual es el resultado

var numbers = [5, 32, 43, 4];

//El resiltado será un arreglo con los valores [5, 43], debido a que
// "%" evalua el restante de una división, al tener la expresión:
//n % 2 !== 0; se está pidiendo que se retornen los valores cuyo
//resultado de la divisón sean numeros impares
let resultFilter = numbers.filter((n) => {
  return n % 2 !== 0;
});

console.log(resultFilter);

//3. Dado el siguiente arreglo de objetos, genera una función en js
// que filtre las personas menores de 35 años.

var people = [
  {
    id: 1,
    name: 'John',
    age: 28,
  },
  {
    id: 2,
    name: 'Jane',
    age: 31,
  },
  {
    id: 3,
    name: 'Peter',
    age: 55,
  },
];

var filteredPeople = people.filter((person) => person.age < 35);
console.log(filteredPeople);

//4. Dado el siguiente arreglo de objetos
// Genera la función para imprimir en consola el numero máximo de
// veces que aparece cada nombre.

let personas = [
  {
    name: 'bob',
    id: 1,
  },
  {
    name: 'john',
    id: 2,
  },
  {
    name: 'alex',
    id: 3,
  },
  {
    name: 'john',
    id: 3,
  },
  {
    name: 'isaak',
    id: 4,
  },
];

let nameOcurr = function (arr) {
  let ocurrences = [];
  arr.map((element) => {
    // ocurrences[element.name] = isNaN(ocurrences[element.name])
    //   ? 1
    //   : (ocurrences[element.name] += 1);
    ocurrences[element.name] = (ocurrences[element.name] || 0) + 1;
  });
  // ocurrences = arr
  //   .map((element) => element.name)
  //   .reduce((count, item) => {
  //     count[item] = (count[item] || 0) + 1;
  //     return count;
  //   });
  return ocurrences;
};
console.log(nameOcurr(personas));

//5. Dado el siguiente arreglo, genera la función para imprimir en consola
// El numero máximo y el número mínimo

var myArray = [1, 2, 3, 4, 35, 25, 7, 1000, 25, 45, 365, 0];
console.log(getMinMax(myArray));

function getMinMax(arr) {
  let min = arr[0];
  let max = 0;
  myArray.forEach((element) => {
    min = element < min ? element : min;
    max = element > max ? element : max;
  });
  return [min, max];
}

//6. Teniendo el siguiente objeto, genera la función para pasar el
// elemento 'key' a un arreglo y ordernarlos según su valor
var object = {
  key1: 10,
  key2: 3,
  key3: 40,
  key4: 20,
};
var arrObject = getMinMax(Object.values(object));
console.log(arrObject);
