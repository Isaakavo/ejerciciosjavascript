//1. Teniendo el siguiente arreglo:
var personArr = [
  {
    personId: 123,
    name: 'jhon',
    city: 'Melbourne',
    phoneNo: '1234567890',
  },
  {
    personId: 124,
    name: 'Amelia',
    city: 'Sydney',
    phoneNo: '1234567890',
  },

  {
    personId: 125,
    name: 'Emily',
    city: 'Perth',
    phoneNo: '1234567890',
  },

  {
    personId: 126,
    name: 'Abraham',
    city: 'Perth',
    phoneNo: '1234567890',
  },
];

const tbody = document.querySelector('#tableBody');

personArr.map((elem) => {
  let row = document.createElement('tr');
  let personIdElem = document.createElement('td');
  let nameElem = document.createElement('td');
  let cityElem = document.createElement('td');
  let phoneNoElem = document.createElement('td');

  personIdElem.innerText = elem.personId;
  nameElem.innerText = elem.name;
  cityElem.innerText = elem.city;
  phoneNoElem.innerText = elem.phoneNo;

  row.appendChild(personIdElem);
  row.appendChild(nameElem);
  row.appendChild(cityElem);
  row.appendChild(phoneNoElem);

  tbody.appendChild(row);
});
//Genera una tabla con javascript en un documento HTML
//que presente los datos

//2. Teniendo el siguiente código en un documento HTML...
//Genera la función javascript para que al dar click en cada elemento
//"li" se muestre un alert con la siguiente información perteneciente
//al elemento.

document.querySelector('ul').addEventListener('click', (e) => {
  alert(
    'The id is: ' +
      e.target.dataset.id +
      ' and the dial code is: ' +
      e.target.dataset.dialCode
  );
});
