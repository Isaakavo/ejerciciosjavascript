//1. Dado el siguiente objeto y arreglo:
var anObject = {
  foo: 'bar',
  length: 'interesting',
  0: 'zero!',
  1: 'one!',
};
var anArray = ['zero', 'one'];

//Cual es el resultado del siguiente codigo y porque ?
console.log(anArray[0], anObject[0]); // ['zero', 'zero!']
console.log(anArray[1], anObject[1]); // ['one', 'one']
console.log(anArray.length, anObject.length); //[ '2', 'interesting']
console.log(anArray.foo, anObject.foo); //['undefined', 'bar']

console.log(typeof anArray == 'object', typeof anObject == 'object'); // [false, true]
console.log(anArray instanceof Object, anObject instanceof Object); //false. true
console.log(anArray instanceof Array, anObject instanceof Array); //true, false
console.log(Array.isArray(anArray), Array.isArray(anObject)); //True, false

//2. Dado el siguiente objeto:
var obj = {
  a: 'hello',
  b: 'this is',
  c: 'javascript!',
};

//Que metodo del objecto 'Object' puedo usar para que me dé el siguiente
//resultado //["hello", "this is", "javascript"]

var arr = Object.values(obj);
console.log(arr);

//3. Crear una función para imprimir en consola la siguiente serie de numeros:
//0, 2, 4... 98
let arrSecuencia = [];
for (let index = 0; index <= 98; index += 2) {
  arrSecuencia.push(index);
}
console.log(arrSecuencia);

//4. Dado el siguiente código:
let zero = 0;
function multiply(x) {
  return x * 2;
}
function add(a = 1 + zero, b = a, c = b + a, d = multiply(c)) {
  console.log(a + b + c, d);
}

//Cual es el resultado de ejecutar las siguientes sentencias?
add(1); // 4, 4
add(3); // 12, 12
add(2, 7); //18, 18
add(1, 2, 5); //8, 10
add(1, 2, 5, 10); // 8, 10

//5. Dado el siguiente código:

class MyClass {
  constructor() {
    this.names_ = [];
  }
  set name(value) {
    this.names_.push(value);
  }
  get name() {
    return this.names_[this.names_.length - 1];
  }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';

//cual es el resultado de ejecutar las siguientes sentencias y porque?

//El resultado es Bob porque el getter está regresnado siempre el ultimo
//elemento del arreglo names_
console.log(myClassInstance.name);
//Regresa el arreglo integro, ya que se esta llamando directamente
//a dicho arreglo
console.log(myClassInstance.names_);

//6. Dado el siguiente codigo:

const classIntance = new (class {
  get prop() {
    return 5;
  }
})();

//Cual es el resultado de ejecutar las siguientes sentencias y porque?

//No hace nada ya que no se está declarando como tal un setter
//ni un atributo llamado prop
classIntance.prop = 10;

console.log(classIntance.prop);

//7. Dado el siguiente codigo:
class Queue {
  constructor() {
    const list = [];
    this.enqueue = function (type) {
      list.push(type);
      return type;
    };
    this.dequeue = function () {
      return list.shift();
    };
  }
}
//Cual es el resultado de ejecutar las siguientes sentencias y porque?
var q = new Queue();

q.enqueue(9);
q.enqueue(8);
q.enqueue(7);

//pinta 9
console.log(q.dequeue());
//Pinta 8
console.log(q.dequeue());
//Pinta 7
console.log(q.dequeue());
//Pinta la clase
console.log(q);
//Pinta las funciones en un arreglo... porque?
console.log(Object.keys(q));

//8. Dada la siguiente clase:
class Person {
  #firstname_;
  #lastname_;
  constructor(firstname, lastname) {
    this.#firstname_ = firstname;
    this.#lastname_ = lastname;
  }

  set firstname(value) {
    this.#firstname_ = value;
  }
  get firstname() {
    return this.#firstname_;
  }

  set lastname(value) {
    this.#lastname_ = value;
  }
  get lastname() {
    return this.#lastname_;
  }
}

//Define los métodos correspondientes para ejecutar el siguiente codigo:
let person = new Person('John', 'Doe');
console.log(person.firstname, person.lastname);
person.firstname = 'Foo';
person.lastname = 'Bar';
console.log(person.firstname, person.lastname);

//9. Dado el siguiente codigo HTMl...
//Agregar el codigo necesario para que el evento click de los objetos
//contenidos en la variable 'deleteBtn' se muestre en ventana de confirmacion
//para eliminar el post(div) de la pagina

var deleteBtn = document.querySelectorAll('[data-deletepost]');
var post102 = document.querySelector('#post-102');
var post103 = document.querySelector('#post-103');

deleteBtn.forEach((a) => {
  a.addEventListener('click', (e) => {
    if (confirm(`Estás seguro que quieres borrar ${e.target}?`)) {
      removeDiv(e.target.dataset.deletepost);
    }
  });
});
function removeDiv(id) {
  if (id === post102.id) {
    post102.remove();
  } else if (id === post103.id) {
    post103.remove();
  }
}

//10. Generar una páguna y el codigo javascript para que se muestre y
//actualice el siguiente recuadro cada segundo:

let hours = 0;
let minutes = 0;
let segs = 0;

const counter = document.querySelector('#counter');

setInterval(() => {
  segs++;
  if (segs >= 60) {
    segs = 0;
    minutes++;
  } else if (minutes >= 60) {
    segs = 0;
    minutes = 0;
    hours++;
  }
  console.log(hours, minutes, segs);
  counter.innerHTML = `${hours} h: ${minutes} min: ${segs} seg`;
}, 1000);
