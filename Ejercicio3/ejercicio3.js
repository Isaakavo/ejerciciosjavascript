//1. Generar una página html y el código js para que:
//a. Al mover el raton en cualquier punto de la ventan del navegador
//, se muestre la posición del puntero respecto del navegador y respecto
//de la página:
// const xCoor = document.querySelector('#xCoor');
// const yCoor = document.querySelector('#yCoor');
const page = document.querySelector('#page');
const screen = document.querySelector('#screen');
window.addEventListener('mousemove', (e) => {
  let coordinates = [e.pageX, e.pageY];
  let screenCoor = [e.screenX, e.screenY];
  page.innerHTML = `Página: [${coordinates}]`;
  screen.innerHTML = `Screen: [${screenCoor}] `;
});

//b. Al pulsar cualquier tecla, el mensaje mostrado debe cambiar para
//indicar el nuevo evento y su información asociada:
const caracter = document.querySelector('#caracter');

window.addEventListener('keydown', (e) => {
  caracter.innerHTML = `Caracter: [${e.key}]`;
});
